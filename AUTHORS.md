# Author contributions

The following people contributed to making of these lecture notes:

* Toeno van der Sar
* Brecht Simon
* Yufan Li
* Yasmin Doedes
* Anton Akhmerov
  
<!--
Execute
git shortlog -s | sed -e "s/^ *[0-9\t ]*//"| xargs -i sh -c 'grep -q "{}" AUTHORS.md || echo "{}"'

To check if any authors are missing from this list.
 -->
