MathJax.Hub.Config({
  config: ["MMLorHTML.js"],
  jax: ["input/TeX", "output/HTML-CSS", "output/NativeMML"],
  tex2jax: {
    inlineMath: [ ['$','$'] ],
    processEscapes: true
  },
  /* added the following to try and get eqn nrs */
  TeX: {
    TagSide: "right",
    TagIndent: ".8em",
    MultLineWidth: "85%",
    equationNumbers: {
      autoNumber: "AMS",
    },
    unicode: {
      fonts: "STIXGeneral,'Arial Unicode MS'"
    }
  },
  displayAlign: 'center',
  showProcessingMessages: false,
  messageStyle: 'none'
  /* END adding try and get eqn nrs */
  extensions: ["MathMenu.js", "MathZoom.js"]
});
