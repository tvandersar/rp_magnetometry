# Writing the report
Writing a report is a central part of this course. Therefore, we strongly recommend that you review [these guidelines](https://surfdrive.surf.nl/files/index.php/s/LxX3dCyJqxnnBho) to see what we expect. An additional good source is [this document](https://nsweb.tn.tudelft.nl/~gsteele/SQUID_practicum/Report%20Writing%20Guidelines%20and%20Advice.html), written by prof. Gary Steele.

In particular, figures are a key element of a report and require significant time investment. Figures should be clear and convey a clear message. Moreover, the **caption should describe in detail what is in the figure**. The reader should be able to understand the complete experiment just by reading your figure captions.

## Report structure and assessment

We will assess the report based on the general grading scheme for the RP practicum (Brightspace/RP practicum/Content). Here, we summarize what we expect in the report for this practicum, while leaving room for your own interpretations:

### Theory

- Describe the NV center and how you can use it for magnetometry (Exercise 1). 
- Include a plot of the theoretical prediction of ESR frequencies vs. applied field. (Exercise 2)

### Methods

- Describe the experimental setup: both the optical setup and how you mount the permanent magnet with respect to the NV centers in the diamond. 
- Include a plot of the expected field for a cylindrical magnet (Exercise 3). 
    
### Results & discussion

- Plots of your measurements that help answer your research question.  

### Figures and captions
Figures should have the proper labels (with units) and should be readable (increase font size if necessary).
If multiple curves are visible, include a legend. 
It is recommended to use software such as Inkscape, Illustrator, or Powerpoint to improve the labels and readability. 
A good caption describes: 

- The message of the figure
- What is displayed on the axes 
- What the different curves in a figure are. 
- All metadata necessary to understand the figure well, e.g.: how did you obtain the data.
