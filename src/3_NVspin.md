# Magnetic sensing with NV centers

In the previous section, we saw that we can initialize and read out the NV spin state optically. In order to use the NV centers as a magnetometer, we need to know quantitatively how the energy of the NV spin states depend on magnetic field. In this section we'll discuss how a magnetic field changes the energy difference between the spin states, and how we can measure this difference experimentally.

## Determining the magnetic field from the energy differences between the spin states

In the previous section we saw that the NV spin has 3 possible spin states. The energies of these spin states depend on both the strength and the direction of an applied magnetic field through the Zeeman interaction. We can determine how these energies depend on the magnetic field by writing down the NV spin Hamiltonian and calculating its eigenenergies. This is a somewhat involved procedure using matrix algebra that we have included in the [optional contents](https://magnetometryrp.quantumtinkerer.tudelft.nl/8_Optional/). Here, we give the main result: mathematical expressions that enable us to determine the magnetic field from the (measurable) energy differences between the spin states. 

We call these energy differences $E_u$ and $E_l$ (subscripts refer to "upper" and "lower"), or in units of frequency, $f_u$ and $f_l$ (where $E_{l,u} = hf_{l,u}$). These **'electron spin resonance' (ESR)** frequencies are functions of both the amplitude $B=|\mathbf{B}|$ and the angle $\theta$ of the magnetic field $\mathbf{B}$ with respect to the NV axis:
$$
f_{u,l} = f_{u,l} (B,\cos{\theta}),
$$
We have plotted these frequencies in Fig. 3.1 as a function of magnetic field for various field orientations.

![alt text](figures/esrfreq.png){ align=center }

**Fig. 3.1 - NV electron spin resonance (ESR) frequencies as a function of magnetic field.**  *The two NV ESR frequencies $f_{u,l}$ depend on both the magnitude $B$ and the angle $\theta$ of the magnetic field $\mathbf{B}$ with respect to the NV axis. As a result, we can extract both $B$ and $\theta$ from a measurement of the ESR frequencies. The splitting between the two ESR frequencies is linear and strongest when the field is aligned with the NV axis (black lines).*

As a special case, when $\mathbf{B}$ is *aligned* with the NV axis (i.e., $\theta = 0$), the above relation reduce to a simple linear form:

$$f_u = D+\gamma B \quad \text{and} \quad f_l = D-\gamma B.$$

as shown in Fig. 3.1 (black lines). Inverting $f_{u,l} = f_{u,l}(B,\theta)$ enables us to determine $B$ and $\theta$ from the ESR frequencies $f_{u,l}$. How to do so for a general value of $\theta$ is described in [optional contents](https://magnetometryrp.quantumtinkerer.tudelft.nl/8_Optional/). The key result is:

$$B= \frac{1}{\sqrt{3}\gamma}\sqrt{f_u^2+f_l^2-f_u f_l-D^2}$$

$$\cos^2\theta = \frac{-(f_u+f_l)^3+3f_u^3+3f_l^3}{27D(\gamma B)^2}+\frac{2D^2}{27(\gamma B)^2}+\frac{1}{3}.$$

Here, $\gamma = 28$ GHz/T is the gyromagnetic ratio of the electron, and $D = 2.87 \mathrm{GHz}$ is the 'zero-field splitting' (i.e., the splitting between $m_S = 0$ and $m_S = \pm 1$ at zero external field). It is important to note that while the last equation tells us the angle $\theta$ of the field with respect to the NV axis, it does not tell us in which direction the component perpendicular to the NV axis points.

## How do we measure the NV ESR frequencies?

To experimentally determine $f_u$ and $f_l$, we apply an oscillating, microwave magnetic field to the diamond using a nearby wire through which we send an oscillating current (note: this *microwave* field is in addition to the *static* magnetic field that we want to measure). We do so under continuous optical illumination so that we continuously drive the NV spins into $m_S=0$ (see previous section). We then sweep the frequency of these microwaves across the expected NV ESR frequencies while monitoring the NV photoluminescence (Fig. 3.2). Once we hit the ESR frequencies $f_u$ and $f_l$, we will drive the NV spins into the dark $m_S = \pm 1$ states, which results in a reduced photoluminescence. Therefore, we can determine $f_u$ and $f_l$ by measuring the photoluminescence intensity as a function of the microwave frequency (the "ESR spectrum") and reading out the location of the photoluminescence dips, as shown in Fig. 3.2.

![Image title](figures/NVlevelwithESR.png){ align=center }

**Fig. 3.2 - Optical detection of the NV electron spin resonance frequencies.** *The ground state $|g\rangle$ of
the NV center has three spin states with a zero-field splitting $D$ = 2.87 GHz between the $m_s=0$ and the $m_s=\pm1$ states. The spin states split in a magnetic field $B$ as governed by the electron gyromagnetic ratio $\gamma$. Green-laser excitation leads to a red photoluminescence with an intensity that depends on the spin state, as indicated by the width of the red arrows. This spin-dependent photoluminescence enables us to detect the splitting between the NV spin states,  and thereby the magnetic field $B$, under the application of microwaves that drive NV spin transitions, as indicated on the right.*

From the two NV ESR frequencies, we are able to extract the magnitude and the angle of the magnetic field relative to the NV axis, as described above. However, these two frequencies do not fully specify the direction of the magnetic field yet. In particular, we cannot extract the component of the field *perpendicular* to the NV axis. Fortunately, we have four different crystallographic orientations of the NV centers, each of which generally makes a different angle $\theta$ with respect to the magnetic field. As such, we generally expect eight different ESR frequencies, yielding an overdetermined set of equations that *does* enable us to fully reconstruct the orientation of the magnetic field, albeit still only up to the four-fold symmetry of the diamond lattice. See the [optional contents](https://magnetometryrp.quantumtinkerer.tudelft.nl/8_Optional/) for details on this.

 Because we have four different NV orientations, we generally expect four different sets of $\{ f_u,f_l\}$, resulting in a maximum of eight dips in the ESR spectrum. However, there are situations in which fewer dips are visible in the ESR spectrum. This happens for instance when the projection of the magnetic field onto the NV axis is identical for two or more NV orientations (resulting in overlapping dips in the ESR spectrum). Furthermore, the dips become less pronounced with increasing field angle $\theta$. Finally, the polarization of the excitation laser plays a role. These last two points are due to NV physics that we will not describe in detail here. The general message is that there are various reasonas that some dips may be more obscured by noise in the data than others.

### Exercise 3 - Analyze the behavior of ESR spectrum in different configurations

 In the lab sessions of this practicum, you will mostly be working in one of the following magnetic-field configurations. For each of them, calculate the projection of the field on the four NV orientations and determine how many dips you expect in the ESR spectrum:

 1. There is no external field 
 2. The external field is in the $\langle 100 \rangle$ direction of the diamond 
 3. The external field is in the $\langle 110 \rangle$ direction of the diamond 
 4. The external field lies in the $\langle 100 \rangle$ plane with an arbitrary rotation (i.e. in the direction of $(0,y,z)$) 
 5. The external field is in the $\langle 111 \rangle$ direction of the diamond (i.e. aligned with one orientation of NV centers) 
