## Welcome to day 1 of the Research Practicum: Magnetometry with spins in diamond

Today we start with the first experiments!
We will guide you through this first day, to help you 
learn the basics of NV magnetometry and finally to help use your time 
effectively for the rest of the practicum.

!!! success "Expected prerequisites"

    Before the start of day 1 we expect you to have prepared the following:

    -  Have read pages: 
        * Learning goals
        * The NV center in diamond
        * Optical initialization and readout of NV spins
        * The NV spin Hamiltonian,
        * The fluorescence microscope
    -   Have access to a python or matlab installation to be able to:
        * Answer the questions from the manual
        * Analyze the data from home

!!! summary "Learning goals"
    
    After the first day you will able to:

    - Explain the principles of NV magnetometry
    - Formulate a research question
    - Make a planning for the next days
    - Write an outline for your report


## Instructions day 1: 
### Technical instructions
#### 1. Starting up MATLAB and the Graphical User Interface (GUI)
- Open MATLAB 2022a
- Right-click the NVAFM folder (located in C:) and select 'Add to Path' --> 'Selected Folders and Subfolders'
- Open the GUI by typing 'NV_CameraESR' in the MATLAB command line  
#### 2. Using the Camera
- Open communication with camera. Now click **continuous** acquisition mode: what do you see?
- Turn on the laser. Stop the continuous acquisition and take a **snapshot** : what do you see and why? 
- The camera reads out the CCD line by line, to speed up the measurement, you can change the "hardware" region of interest (ROI). You can select a region in the CameraImage by clicking on the magnifying glass/zoom and then dragging a rectangle. Next, click 'Set view to ROI' in the left panel. 
- To remove contribution of pixels where NVs are not excited, you can change the "software" region of interest. Again, drag a rectangle in the CameraImage. Then click: "Set view to software ROI". You should see the region you selected in a red rectangle.
- We can now try to configure the continuous acquisition settings:
    * A short exposure time will speed up the measurement and reduce time dependent drift
    * A too short exposure time unfortunately leads to lost frames in the serial communication with the camera

#### 2. Setting up an Electron Spin Resonance measurement
- Open the communication with the RF_WF microwave source and also click 'On' in the MW Trigger mode frame.
- What are the ESR frequency values you expect? 
    * Calculate or estimate the expected frequency range where you expect the ESR frequency depending on the position of the magnet in the setup ([the NV spin Hamiltonian](https://magnetometryrp.quantumtinkerer.tudelft.nl/3_NVspin/))
    * Fill out the ESR Sweep values (GHz): (lowest value, highest value, Npoints). 
    * To determine the amount of points (Npoints in the linspace), consider the frequency differences you want to resolve. 
- Make sure the microwave power is set to **its maximum value: 19 dBm**
- If the signal is noisy:
    * Choose a higher number of averages (Nreps) 
    * Select normalize, this functionality allows to normalize each datapoint with an additional measurement at a single frequency (NormFreq) to overcome time dependent fluctuations in the signal. Make sure that this is a frequency where you do not expect a dip in PL. 
- All individual frequency traces are recorded in a large array (Nreps x Npoints) that is automatically plotted in the tab ESRsweep.  In the graph at the bottom, only the average signal over the repetitions that were done so far is shown.
- START CAMERA ESR to start the measurement and STOP (in Camera communication tab) to stop the measurement before the final repitition is finished. When you clicked STOP, you need to wait until the frequency sweep of that particular repetition is finished before doing anything else. 

#### 3. Plotting and saving 
- Click Save (top left) to save the data to the local folder: C:\NVAFM\camera_data, it is now saved as a Matlab struct
- You can load a previously saved dataset for visualization or check the used parameters using Load/Load recent.
- In the Fitting & Saving tab, you can also fit the ESR spectrum with lorentzian functions to extract the spectrum properties: the peak center frequencies, peak contrast, S2N, and the Full Width at Half Maximum. 
- If the fits are bad, try tweaking the Min. peak width and Max. peak width fit settings. Note these values are in MHz, not GHz. 
- If you want to force fit the number of peaks you have entered, you need to change the Significance value to 0. Otherwise, it first tries to fit the number you have entered but if a different number of peaks gives better fit results it will resort to this. 
- Press Create Figure and subsequently Save to datafile & ppt, this will copy the figure to a (new) powerpoint presentation together with the acquisition parameters which you can use as a logbook for your experiments. Make use of it! At the same time it also saves the ESR sweep data to a **simple .csv file** that you can easily import using standard python libraries for further analysis
- At the end of the day **make a personal copy** of all your datafiles.    

### Magnetometry experiments
#### 1. Zero field measurement
Perform a measurement with the magnet far away from the diamond. Describe the ESR spectrum. What is the signal to noise? Can you improve it by changing experimental parameters such as Nreps, Npoints, Gain?

#### 2. Influence of a magnet
- Place a magnet behind the diamond
- Describe the corresponding ESR spectrum:
    *  How many peaks do you observe and why?
- Measure the distance between the permanent magnet and diamond
- Estimate the approximate magnetic field strength measured at the position of the diamond, given the field profile of a cylindrical magnet [exercise 3.1](https://magnetometryrp.quantumtinkerer.tudelft.nl/4_SampleAndSetup/)
- Consider the angle of the field with respect to the different [NV center orientations](https://magnetometryrp.quantumtinkerer.tudelft.nl/1_NVbackground/) 


#### 3. Determine your research question and make a measurement/report outline for the next measurement days
- Vary distance between magnet and diamond
- Vary angle 
- Reconstruct the vector magnetic field from a dataset with at least 3 different pairs of NV center peaks. [Assignment 2.3](https://magnetometryrp.quantumtinkerer.tudelft.nl/3_NVspin/) 
