# Magnetometry with spins in diamond

Measuring magnetic fields plays a central role in technologies ranging from medical imaging to quantum devices. In this lab course you will study one of the most advanced magnetic-sensing technologies of today: magnetometry based on the electronic spin of nitrogen-vacancy (NV) centers in diamond. 

The magnetic moment of elementary particles is called spin. The energy of a spin is sensitive to the magnetic field because of the Zeeman interaction. This interaction enables us to use spins as magnetic-field sensors. In this practicum, you will use a fluorescence microscope to measure the electron-spin resonance (ESR) of NV centers, from which you will determine both the amplitude and direction of an applied magnetic field.

!!! summary "Learning goals"

    After this lab course you will be able to:

    - Describe the optical and spin properties of the nitrogen-vacancy (NV) center in diamond.
    - Measure magnetic fields with an NV-diamond in a fluorescence microscope setup.
    - Formulate a research question that can be addressed with this setup, and perform measurements to do so.  
    - Write a scientific report about the results.

Writing a report is a central part of this course. Therefore, we strongly recommend that you review [these guidelines](https://surfdrive.surf.nl/files/index.php/s/LxX3dCyJqxnnBho) to see what we expect. An additional good source is [this document](https://nsweb.tn.tudelft.nl/~gsteele/SQUID_practicum/Report%20Writing%20Guidelines%20and%20Advice.html), written by prof. Gary Steele.)

In these notes our aim is to provide learning materials which are:

- self-contained
- easy to modify and remix, so we provide the full source, including the code
- open for reuse: see the license below.

Whether you are a student taking this course, or an instructor reusing the materials, we welcome all contributions, so check out the [course repository](https://gitlab.kwant-project.org/tvandersar/rp_magnetometry), especially do [let us know](https://gitlab.kwant-project.org/tvandersar/rp_magnetometry/issues/new?issuable_template=typo) if you see a typo!
