# The NV center in diamond

In this section, we introduce the nitrogen-vacancy (NV) center in diamond and discuss the possible atomic orientations of the NV centers inside the diamond lattice. The NV orientations play an important role in the response of the NV spins to magnetic field (discussed in next sections).

## NV centers and their orientations in the diamond lattice

The NV center is a lattice defect consisting of a substitutional nitrogen atom next to a vacancy (an empty site) in the diamond carbon lattice. The NV axis is defined as the vector from the vacancy towards the nitrogen atom, when the vacancy is taken as the origin of your coordinate system. Because of the crystal structure, there are 4 possible orientations of the NV axis: the $111$, $1 \overline{1} \overline{1}$, $\overline{1} 1 \overline{1}$, and the $\overline{1} \overline{1} 1$ directions (Fig. 1.1).

![NV lattice structure](figures/four_possible_NV_orientations.png)

**Fig. 1.1 - The four possible orientations of nitrogen-vacancy (NV) centers in the diamond carbon lattice.** *The NV center consists of a substitutional nitrogen (N) atom next to a vacancy (V) in the diamond carbon lattice. The figure shows the four possible crystallographic orientations of the NV centers. The numbers between square brackets indicate the direction of the N-V axis: for instance, $[111]$ indicates that the N-V axis points along $\mathbf{r} = 1\mathbf{\hat{x}}+1\mathbf{\hat{y}}+1\mathbf{\hat{z}}$. The overbar indicates a minus sign, so that $[1\overline{1}\overline{1}]$ corresponds to $\mathbf{r} = 1\mathbf{\hat{x}}-1\mathbf{\hat{y}}-1\mathbf{\hat{z}}$. 
 Image from [Pham et al.](https://www.researchgate.net/publication/229156655_Enhanced_metrology_using_preferential_orientation_of_nitrogen-vacancycenters_in_diamond)*

  Given the crystal structure, we conclude that if we know the orientation of our diamond crystal, we know in which direction the NV centers are pointing inside it. This allows us to determine the orientation of NV centers in our setup.

#### Exercise 1: Orientation of NV centers in diamond

In this practicum we use a synthetic, rectangular diamond crystal. All surfaces of the diamond belong to the "$\{001\}$ family of lattice planes". The $\{001\}$ notation is called [Miller index notation](https://en.wikipedia.org/wiki/Miller_index) and indicates that the surface normals of these planes point in the $x$, $y,$ and $z$ directions.
  
  1. Using this knowledge, calculate the 4 possible angles of the NV axis with respect to the surfaces of this diamond.
