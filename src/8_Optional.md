In this section, we derive the NV electron spin resonance (ESR) frequencies as a function of magnetic field. We also derive the inverse relation that enables us to extract the magnetic field from measured ESR frequencies.

## The NV spin Hamiltonian 
The eigenenergies of the NV spin are given by the eigenvalues of the NV spin Hamiltonian
$$
H = DS_z^2+\gamma\mathbf{B}\cdot\mathbf{S}.
$$
Here, the coordinate system is chosen such that the z-axis is parallel to the N-V axis. Furthermore, $\gamma$=28 GHz/T the electron gyromagnetic ratio, $D=2.87$ GHz is the zero-field splitting, and $\mathbf{S}=(S_x,S_y,S_z)$ are the Pauli spin matrices for a spin-1 system:

$$
S_x= \frac{1}{\sqrt{2}} 
\begin{pmatrix}
0 & 1 & 0\\
1 & 0 & 1\\
0 & 1 & 0
\end{pmatrix}, \quad
S_y= \frac{1}{\sqrt{2}}
\begin{pmatrix}
0 & -i & 0\\
i & 0 & -i\\
0 & i & 0
\end{pmatrix},  \quad
S_z= \begin{pmatrix}
1 & 0 & 0\\
0 & 0 & 0\\
0 & 0 & -1
\end{pmatrix}.
$$

Note that we express the NV spin Hamiltonian $H$ in units of frequency, which allows a direct comparison to the frequency of the microwaves we use to drive the NV spin transitions.

#### Exercise A.1 - Calculating the NV spin eigenfrequencies vs an applied magnetic field
    
  1. Derive an expression for the eigenfrequencies of $H$ as a function of a magnetic field $B$ applied _along the NV axis_ - e.g. $\mathbf{B} = B \hat{z}$
  2. Numerically diagonalize the Hamiltonian $H$ (using e.g. Matlab or Python) to plot the eigenfrequencies as a function of a magnetic field $B$ oriented _at an angle $\theta$ w.r.t. the NV axis_.

#### Exercise A.2 - Calculating the NV electron spin resonance (ESR) frequencies

Transitions between NV spin states that differ by $\Delta m_s=1$ can be driven by applying an oscillating magnetic field of which the frequency matches the difference between the eigenfrequencies. These resonance frequencies are called the electron spin resonance (ESR) frequencies.

  1. From the results of assignment 3.2.1, plot the NV ESR frequencies as a function of a magnetic field that is applied along the NV axis.
  2. From the results of assignment 3.2.2, plot the NV ESR frequencies as a function of a magnetic field that is applied perpendicularly to the NV axis.

## Extracting the magnetic field from measured ESR frequencies
We will now describe how the magnetic field can be determined from a measurement of the NV ESR frequencies. To do so, we first derive the characteristic polynomial of $H$ to express its eigenfrequencies in terms of $B$, $\theta$, and $D$, and then solve for $B$ and $\theta$ in terms of the ESR frequencies.

### Characteristic polynomial
It is convenient to define a new Hamiltonian $H'=H-\frac{2}{3}DI$ so that $\text{Tr}(H')=0$. Note that this simply subtracts a constant $2D/3$ from the eigenfrequencies and hence does not change the frequency differences. The reason for doing this is that it results in a relatively succinct characteristic polynomial. 

We take $\mathbf{B}=B(\sin \theta \mathbf{\hat{x}}+\cos \theta \mathbf{\hat{z}})$, giving:
$$
H'= \begin{pmatrix}
\frac{D}{3} + \gamma B \cos\theta & \frac{\gamma} {\sqrt{2}} B \sin(\theta) & 0 \\
\frac{\gamma }{\sqrt{2}} B \sin(\theta) & -\frac{2D}{3} &  \frac{\gamma }{\sqrt{2}} B \sin(\theta)\\
0 & \frac{\gamma }{\sqrt{2}} B \sin(\theta) & \frac{D}{3}- \gamma B \cos(\theta)\\
\end{pmatrix}
$$
The eigenfrequencies $f_{i=1,2,3}$ of $H'$ can be found from its characteristic polynomial:
$$
f^3-(\frac{D^2}{3}+(\gamma B)^2)f-\frac{1}{2}D(\gamma B)^2\cos(2\theta)-\frac{1}{6}D(\gamma B)^2+\frac{2}{27}D^3=0
\label{eq:charpol}
$$
which should factorize according to
$$
\begin{align}
(f-f_1)(f-f_2)(f-f_3) \\
=f^3-f^2(f_1+f_2+f_3)+f(f_1f_2+f_1f_3+f_2f_3)-f_1f_2f_3 = 0.
\end{align}
\label{eq:sol}
$$
By comparing these last two equations we can express $B$ and $\theta$ as a function of $f_{i=1,2,3}$.

### Extracting the magnetic field from a pair of NV ESR frequencies
Comparing the _quadratic_ terms, it follows that 
$$
f_1+f_2+f_3=0
\label{eq:sumfreq}
$$
The main ESR frequencies are given by $f_u=f_3-f_1$ and $f_l=f_2-f_1$. We define the following useful relations:
$$
f_1=\frac{-f_u-f_l}{3}, \quad f_2 = \frac{1}{3}(2f_l-f_u), \quad f_3=\frac{1}{3}(2f_u-f_l).
$$

Comparing the _linear_ terms, it follows that
$$
\begin{align}
\gamma B=\frac{1}{\sqrt{3}}\sqrt{f_u^2+f_l^2-f_uf_l-D^2}
\end{align}
\label{eq:B}
$$

Finally, comparing the _constant_ terms, it follows that
$$
\cos^2(\theta) = \frac{f_1f_2f_3}{D(\gamma B)^2}+\frac{2D^2}{27(\gamma B)^2}+\frac{1}{3} = \frac{-(f_u+f_l)^3+3f_u^3+3f_l^3}{27D(\gamma B)^2}+\frac{2D^2}{27(\gamma B)^2}+\frac{1}{3}
$$

### Extracting the vector magnetic field using all 4 NV orientations
The 4 possible NV orientations in the diamond lattice are $111$, $1\overline{1}\overline{1}$,  $\overline{1} 1 \overline{1}$, and  $\overline{1}\overline{1} 1$. A measurement of the ESR frequencies of all 4 orientations allows us to extract the direction $\mathbf{\hat{b}}$ of the magnetic field w.r.t. the diamond lattice. We do so by solving the system of equations $\mathbf{\hat{n}_{NV_i}}\cdot \mathbf{\hat{b}}=\cos(\theta_i)$, with $i=1,2,3,4$. In matrix form $N\mathbf{\hat{b}}=\mathbf{c}$ 

with

$$ N=
\frac{1}{\sqrt{3}}
\begin{pmatrix}
1 & 1 & 1\\
-1 & -1 & 1\\
-1 & 1 & -1\\
1 & -1 & -1\\	
\end{pmatrix} \quad
\text{and} \quad
\mathbf{c} = 
\begin{pmatrix}
\cos{\theta_1}\\
\cos{\theta_2}\\
\cos{\theta_3}\\
\cos{\theta_4}\\
\end{pmatrix}
$$

This is an overdetermined system of equations having a least-square solution

$$
\mathbf{\hat{b}} = (N^TN)^{-1}N^T\mathbf{c}
$$

#### Exercise A.3 - Checking the expressions for the magnetic field and its angle.
  1. From the ESR frequencies calculated in assignment 3.3, determine the values of $B$ and $\theta$ using the equations just derived.


## Summary
* We have numerically diagonalized the Hamiltonian to study the behavior of the ESR frequencies as a function of $B$ and its angle $\theta$.
* We have shown how to extract $B$ and $\theta$ from a measurement of the NV ESR frequencies.
* We have shown how to extract the orientation of the magnetic field w.r.t. diamond crystal.


