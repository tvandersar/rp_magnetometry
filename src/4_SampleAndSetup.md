# The fluorescence microscope

Detecting magnetic fields using NV centers requires the ability to excite the NV centers optically and detecting their spin-dependent photoluminescence under microwave driving, as described in the previous sections. Here we introduce the experimental setup.

## Optical excitation and detection of the NV photoluminescence under microwave driving

To detect the electron spin resonance (ESR) frequencies of the NV centers, we use the fluorescence microscope sketched in Fig. 4.1.

![setup](figures/NVsetup_camera.png){ width=60% }

**Fig. 4.1 - Schematic illustration of the fluorescence microscope used for the detection of the NV electron spin resonance frequencies.** *A green laser is focused into a diamond using a microscope objective to excite NV photoluminescence. A dichroic mirror (dark grey rectangle) reflects green light while transmitting red photoluminescence (red arrow), thus acting as an optical filter. The photoluminescence is detected using a CMOS camera. A microwave generator generates an oscillating current that is passed through a wire in close vicinity of the NV centers. This current generates a microwave magnetic field that drives the NV spin transitions.*

A green laser is reflected off a dichroic mirror and focused into the diamond by a microscope objective, thereby exciting NV centers in the diamond. The resulting NV center photoluminescence is collected by the same microscope objective, transmitted through the dichroic mirror and focused onto the sensor chip of an CMOS camera. 

A microwave source generates a microwave current that is delivered to the NV centers by a wire placed on top of the diamond surface, creating the oscillating magnetic fields to drive the NV spin transitions. 

### Exercise 4 - Calculating the magnetic field of a cylindrical magnet

We apply a magnetic field using a small cylindrical magnet on a translation stage. This magnet has a magnetization $M_s= 0.955 \cdot 10^6$ A/m, a length $L=20$ mm, and a diameter $d=10$ mm.  

  1. Derive an expression for the magnetic field of this magnet along a line through its axis as a function of the distance $z$ to its center.

