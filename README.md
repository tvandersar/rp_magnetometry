# Magnetometry with spins in diamond

Lecture notes and teaching material used for the Delft University of Technology course awesome course.

The compiled materials are available at https://magnetometryrp.quantumtinkerer.tudelft.nl

## Origin and technical support

This repository is based on a template for publishing lecture notes, developed
by Anton Akhmerov, who also hosts such repositories for other courses.

## To build the mkdocs on a local computer
The idea is to pull the repository and install the required python and markdown packages in a conda environment

- pull the repository into a directory NVmagnetometry
- install miniconda
- Create a virtual environment in the NVmagnetometry folder and install the various mkdocs and python packages 

```sh
conda create -n NVmagnetometry python=3.10
conda activate NVmagnetometry
pip install -r requirements.txt
conda install -c conda-forge nb_conda  
conda install -c conda-forge cairosvg 
```

Now you can run the website server using

```sh
mkdocs serve
```